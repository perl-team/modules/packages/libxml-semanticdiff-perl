libxml-semanticdiff-perl (1.0007-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

  [ Jenkins ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Debian Janitor ]
  * Update lintian override info format in d/source/lintian-overrides on line 2-7.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 01 Dec 2022 11:40:12 +0000

libxml-semanticdiff-perl (1.0007-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org.

  [ gregor herrmann ]
  * Update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Jonas Smedegaard ]
  * Simplify rules.
    Stop build-depend on cdbs.
  * Update watch file: Rewrite usage comment.
  * Stop build-depend on dh-buildinfo.
  * Mark build-dependencies needed only for testsuite as such.
  * Declare compliance with Debian Policy 4.3.0.
  * Enable autopkgtest.
  * Set Rules-Requires-Root: no.
  * Wrap and sort control file.
  * Update copyright info: Extend coverage of packaging.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 21 Feb 2019 17:30:16 +0100

libxml-semanticdiff-perl (1.0005-1) unstable; urgency=medium

  [ upstream]
  * New release.
    + Convert the distribution to use git, GitHub, and Dist-Zilla.
    + Correct some spelling errors and add more tests.
    + Remove trailing whitespace.

  [ Jonas Smedegaard ]
  * Modernize Vcs-* fields:
    + Consistently use https protocol.
    + Consistently use git (not gitweb) path.
    + Consistently include .git suffix in path.
  * Declare compliance with Debian Policy 4.1.0.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Update watch file:
    + Bump to file format 4.
    + Watch only MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
    + Use substitution strings.
  * Modernize cdbs:
    + Drop get-orig-source target: Use gbp import-orig --uscan.
    + Do copyright-check in maintainer script (not during build).
    + Relax to build-depend unversioned on cdbs.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage for myself.
    + Extend coverage for main upstream author.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Use debhelper compatibility level 9 (not 8).

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 14 Sep 2017 10:14:05 +0200

libxml-semanticdiff-perl (1.0004-2) unstable; urgency=medium

  * Update copyright info:
    + Fix cover convenience code copy of Test::Run.
    + Extend coverage of packaging to include current year.
  * Fix Build-depend explicitly on libmodule-build-perl, and also
    build-depend on (recent perl or) a recent version (see bug#752989).
  * Update watch file:
    + Use www.cpan.org and metacpan.org URLs (replacing search.cpan.org
      URL).
    + Tighten version regex.
  * Fix use canonical Vcs-Git URL.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Jul 2014 18:57:44 +0200

libxml-semanticdiff-perl (1.0004-1) unstable; urgency=medium

  [ upstream]
  * New release.
    + Fix case problem with "repository" in "Build.PL".
    + Remove trailing space.
    + Convert "Changes" file to use CPAN::Changes.
    + Add separate LICENSE file.
    + Add inc/Test/Run/Builder.pm.
    + Update the contact info for Shlomi Fish from the old one.

  [ Jonas Smedegaard ]
  * Stop needlessly mangling upstream version numer.
  * Bump standards-version to 3.9.5.
  * Bump to debhelper compatibility level 8.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 10 May 2014 17:43:19 +0200

libxml-semanticdiff-perl (1.00.00-2) unstable; urgency=low

  [ gregor herrmann ]
  * Remove debian/source/local-options: abort-on-upstream-changes and
    unapply-patches are default in dpkg-source since 1.16.1.

  [ Jonas Smedegaard ]
  * Update package relations:
    + Relax to (build-)depend unversioned on cdbs, debhelper and
      devscripts: Needed versions satisfied in stable, and oldstable no
      longer supported.
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, and referring to
    CDBS+git-buildpackage wiki page for details.
  * Use canonical hostname (anonscm.debian.org) in Vcs-Browser URI.
  * Bump standards-version to 3.9.4.
  * Update copyright file:
    + Bump file format to 1.0.
    + Shorten comments, quote licenses in them, and fix refer to
      explicit (versioned) licenses.
    + Fix list each license as separate License section.
    + Fix use comment and license pseudo-sections to obey silly
      restrictions of copyright format 1.0.
  * Bump packaging license to GPL-3+, and extend copyrigt coverage for
    myself to include recent years.

  [ Salvatore Bonaccorso ]
  * Use canonical hostname (anonscm.debian.org) in Vcs-Git URI.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 01 Jul 2013 12:18:47 +0200

libxml-semanticdiff-perl (1.00.00-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#612604.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 09 Feb 2011 16:01:20 +0100
